(ns twitterandweather.config
  (:require [clojure.tools.logging :as log])
  (:import  [java.util Properties]
            [java.io FileInputStream]
            )
  (:use [clojure.walk])
  (:use korma.core)
  (:use korma.db))

(def app-configs (atom {}))

(defn- config-file []
  (let [result (let [result (System/getenv "surrealsecurities")]
                 (when result (java.io.File. result)))]
    (if (and result (.isFile result))
      result
      (do (log/fatal (format "serverConfig(%s) = nil" result))
          (throw (Exception. (format "Server configuration file (%s) not found." result)))))))

(defn load-config [config]
  (let [properties (Properties.)
        fis (FileInputStream. config)]
    ; populate the properties hashmap with values from the output stream
    (.. properties (load fis))
    (keywordize-keys (into {} properties))))

(defn config-value [name & args]
  (let [value (@app-configs name)]
    (when value
      (let [args (when args (apply assoc {} args))
            {type :type} args
            args (dissoc args :type)
            value (if (vector? value)
                    (loop [x (first value)
                           xs (next value)]
                      (let [properties (dissoc x :value)]
                        (if (or (and (empty? args)
                                     (empty? properties))
                                (and (not (empty? args))
                                     (every? (fn [[k v]]
                                               (= (properties k) v))
                                             args)))
                          (x :value)
                          (when xs
                            (recur (first xs) (next xs))))))
                    value)]
        (when value
          (let [value #^String value]
            (cond (or (nil? type) (= type :string)) value
                  ;; ---
                  (= type :int) (Integer/valueOf value)
                  (= type :long) (Long/valueOf value)
                  (= type :bool) (contains? #{"yes" "true" "y" "t" "1"}
                                            (.toLowerCase value))
                  (= type :path) (java.io.File. value)
                  (= type :url) (java.net.URL. value)
                  )))))))

(defn initialize-config []
  (log/info "initializing configurations..")
  (reset! app-configs (load-config (config-file)))

  ;;accesses databases
  (def accesses-db-host (config-value :accesses-db-host))
  (def accesses-db-port (config-value :accesses-db-port))
  (def accesses-db-name (config-value :accesses-db-name))
  (def accesses-db-user (config-value :accesses-db-user))
  (def accesses-db-pass (config-value :accesses-db-password))
  (def accesses-db-subprotocol (config-value :accesses-db-subprotocol))
  (def accesses-db-subname (config-value :accesses-db-subname))


  ;;traffic tweets database
  (def traffictweets-db-host (config-value :traffictweets-db-host))
  (def traffictweets-db-port (config-value :traffictweets-db-port))
  (def traffictweets-db-name (config-value :traffictweets-db-name))
  (def traffictweets-db-user (config-value :traffictweets-db-user))
  (def traffictweets-db-pass (config-value :traffictweets-db-password))
  (def traffictweets-db-subprotocol (config-value :traffictweets-db-subprotocol))
  (def traffictweets-db-subname (config-value :traffictweets-db-subname))

  ;weather
  (def weather-db-host (config-value :weather-db-host))
  (def weather-db-port (config-value :weather-db-port))
  (def weather-db-name (config-value :weather-db-name))
  (def weather-db-user (config-value :weather-db-user))
  (def weather-db-pass (config-value :weather-db-password))
  (def weather-db-subprotocol (config-value :weather-db-subprotocol))
  (def weather-db-subname (config-value :weather-db-subname))


  (println "acc_user --> "accesses-db-user)
  (println "acc_pass --> "accesses-db-pass)
  (println "acc_subp --> "accesses-db-subprotocol)
  (println "acc_subn --> "accesses-db-subname)



  (println "tt_user --> "traffictweets-db-user)
  (println "tt_pass --> "traffictweets-db-pass)
  (println "tt_subp --> "traffictweets-db-subprotocol)
  (println "tt_subn --> "traffictweets-db-subname)

  (println "weather_user --> "weather-db-user)
  (println "weather_pass --> "weather-db-pass)
  (println "weather_subp --> "weather-db-subprotocol)
  (println "weather_subn --> "weather-db-subname)


  )


(defn initialize-db []
  (initialize-config)
  (defdb accesses (postgres {:db accesses-db-name
                             :user accesses-db-user
                             :password accesses-db-pass
                             :host accesses-db-host
                             :port accesses-db-port
                             :delimiters ""}))


  (defdb traffic (postgres {:db traffictweets-db-name
                            :user traffictweets-db-user
                            :password traffictweets-db-pass
                            :host traffictweets-db-host
                            :port traffictweets-db-port
                            :delimiters ""}))

  (defdb weather (postgres {:db weather-db-name
                            :user weather-db-user
                            :password weather-db-pass
                            :host weather-db-host
                            :port weather-db-port
                            :delimiters ""}))



  )
