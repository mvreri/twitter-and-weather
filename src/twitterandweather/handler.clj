(ns twitterandweather.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.json :as middleware]
            [twitterandweather.query :as query]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [clj-http.client :as httpclient]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

(defroutes app-routes
;=======================================================================================================================
;TRAFFIC
;=======================================================================================================================
;dont use this for now, will crash system, it is unlimited
(POST "/api/traffic/tweets-total" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (let [nse (query/all-tweets body)]
          (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Stock values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data nse })
          )
        )
      )
    )
  )


(POST "/api/traffic/tweets" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (let [nse (query/all-tweets-limited body)]
          (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Stock values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data nse })
          )
        )
      )
    )
  )


(POST "/api/traffic/tweets-more" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (if (= (get-in body ["nextpage"]) nil)
          (json/write-str {:err-msg "fail" :status 400 :user-data "You have not specified a value for nextpage" })
          (let [nse (query/all-tweets-limited-more body)]
            (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Stock values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data nse })
            ))
        )
      )
    )
  )

;requires a tweet id in the form of "refno"
(POST "/api/traffic/tweets-specific" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (if (= (get-in body ["refno"]) nil)
          (json/write-str {:err-msg "fail" :status 400 :user-data "You have not specified a ref no./tweet ID" })
          (let [nse (query/all-tweets-filter-by-tweetid body)]
            (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Stock values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data nse })
            )
          )
        )
      )
    )
  )



;requires a date in the format "tweetdate"
(POST "/api/traffic/tweets-specific-date" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (if (= (get-in body ["tweetdate"]) nil)
          (json/write-str {:err-msg "fail" :status 400 :user-data "You have not specified a date to filter tweets by" })
          (let [nse (query/all-tweets-filter-by-date body)]
            (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Stock values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data nse })
            )
          )
        )
      )
    )
  )


;=======================================================================================================================
;WEATHER
;=======================================================================================================================

(POST "/api/weather/conditions" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (let [weather (query/current-weather body)]
          (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Weather values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data weather })
          )
        )
      )
    )
  )



(POST "/api/events" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (let [weather (query/current-weather body)]
          (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Weather values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data weather })
          )
        )
      )
    )
  )

(POST "/api/events/next" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (let [weather (query/current-weather body)]
          (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Weather values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data weather })
          )
        )
      )
    )
  )

(POST "/api/events/more" request

  (let [body (:body request)]
    (timbre/info "+++++++++++" body)
    (timbre/info "*****REQUEST******" request)


    (if (= (get-in body ["apikey"]) nil)
      (json/write-str {:err-msg "fail" :status 400 :user-data "Missing API KEY" })
      (if (<  (:apibal (first (query/get-api-limit body)) ) 1)
        (json/write-str {:err-msg "fail" :status 400 :user-data "You have either exhausted your API calls, or this Key is Invalid" })
        (let [weather (query/current-weather body)]
          (json/write-str {:err-msg "success" :status 200 :disclaimer (str "Weather values provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://surrealdata.com/terms/") :user-data weather })
          )
        )
      )
    )
  )


(route/resources "/")
(route/not-found "Not Found")
)


(def app
  (-> app-routes
      (middleware/wrap-json-body)
      (middleware/wrap-json-response)
      (wrap-defaults app-routes)))
