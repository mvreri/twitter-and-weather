(ns twitterandweather.query
  (:require [taoensso.timbre :as timbre]
            [ring.middleware.json :as middleware]
            [clojure.data.json :as json]
            [korma.db :refer :all]
            [twitterandweather.config :as config]
            [clj-http.client :as httpclient]
            [korma.core :refer :all]))

(config/initialize-db)
(def db-connection-accesses
  {:classname "org.postgresql.Driver"
   :subprotocol config/accesses-db-subprotocol
   :user config/accesses-db-user
   :password config/accesses-db-pass
   :subname config/accesses-db-subname
   })

(def db-connection-tweets
  {:classname "org.postgresql.Driver"
   :subprotocol config/traffictweets-db-subprotocol
   :user config/traffictweets-db-user
   :password config/traffictweets-db-pass
   :subname config/traffictweets-db-subname
   })


(def db-connection-weather
  {:classname "org.postgresql.Driver"
   :subprotocol config/weather-db-subprotocol
   :user config/weather-db-user
   :password config/weather-db-pass
   :subname config/weather-db-subname
   })

(defn get-api-limit [body]
  ;making sure that the accesses by a given api is always limited to the subscribed plan
  (with-db db-connection-accesses (exec-raw (format "SELECT COALESCE(SUM(p.maxcalls) - COUNT(a.id), 1) AS apibal
                                                      FROM tbl_subscription s
                                                      LEFT JOIN tbl_apicalls a
                                                      ON a.subid = s.id
                                                      INNER JOIN tbl_plans p
                                                      ON p.id = s.planid
                                                      WHERE s.apikey='%s' AND EXTRACT (month from (date(a.calldatetime))) = EXTRACT(month FROM CURRENT_DATE);" (get-in body ["apikey"])) :results))
  )

(defn all-tweets [body]
  ;update the api accesses table
  (with-db db-connection-accesses (exec-raw (format "INSERT INTO tbl_apicalls(subid) SELECT id FROM tbl_subscription WHERE apikey='%s';" (get-in body ["apikey"]))))
  (with-db db-connection-accesses (exec-raw (format "UPDATE tbl_apicalls SET iscorrect=1, postedstring='%s' WHERE id=(SELECT MAX(id) FROM tbl_apicalls);" (json/write-str body))))
  ;get all the records for the last 1 year
  (with-db db-connection-tweets (exec-raw (format "SELECT t.t_tweetid tweetid, t.tweet tweet, t.tweetdate::text tweet_date, a.handle handle
                                                    FROM t_tweets t
                                                    INNER JOIN t_accounts a
                                                    ON a.id = t.acc_id
                                                    ORDER BY t.id DESC;") :results))
  )

;to get the tweets segmenteed
(defn all-tweets-limited-more [body]
  ;update the api accesses table
  (with-db db-connection-accesses (exec-raw (format "INSERT INTO tbl_apicalls(subid) SELECT id FROM tbl_subscription WHERE apikey='%s';" (get-in body ["apikey"]))))
  (with-db db-connection-accesses (exec-raw (format "UPDATE tbl_apicalls SET iscorrect=1, postedstring='%s' WHERE id=(SELECT MAX(id) FROM tbl_apicalls);" (json/write-str body))))
  ;get all the records for the last 1 year
  (with-db db-connection-tweets (exec-raw (format "SELECT t.id tid, t.t_tweetid tweetid, t.tweet tweet, t.tweetdate::text tweet_date, a.handle handle
                                                    FROM t_tweets t
                                                    INNER JOIN t_accounts a
                                                    ON a.id = t.acc_id
                                                    WHERE t.id < '%s'
                                                    ORDER BY t.id DESC
                                                    LIMIT 5;" (get-in body ["nextpage"])) :results))
  )

(defn all-tweets-limited [body]
  ;update the api accesses table
  (with-db db-connection-accesses (exec-raw (format "INSERT INTO tbl_apicalls(subid) SELECT id FROM tbl_subscription WHERE apikey='%s';" (get-in body ["apikey"]))))
  (with-db db-connection-accesses (exec-raw (format "UPDATE tbl_apicalls SET iscorrect=1, postedstring='%s' WHERE id=(SELECT MAX(id) FROM tbl_apicalls);" (json/write-str body))))
  ;get all the records for the last 1 year
  (with-db db-connection-tweets (exec-raw (format "SELECT t.id tid, t.t_tweetid tweetid, t.tweet tweet, t.tweetdate::text tweet_date, a.handle handle
                                                    FROM t_tweets t
                                                    INNER JOIN t_accounts a
                                                    ON a.id = t.acc_id
                                                    ORDER BY t.id DESC
                                                    LIMIT 20;") :results))
  )


(defn all-tweets-filter-by-tweetid [body]
  ;update the api accesses table
  (with-db db-connection-accesses (exec-raw (format "INSERT INTO tbl_apicalls(subid) SELECT id FROM tbl_subscription WHERE apikey='%s';" (get-in body ["apikey"]))))
  (with-db db-connection-accesses (exec-raw (format "UPDATE tbl_apicalls SET iscorrect=1, postedstring='%s' WHERE id=(SELECT MAX(id) FROM tbl_apicalls);" (json/write-str body))))
  ;get all the records for the last 1 year
  (with-db db-connection-tweets (exec-raw (format "SELECT t.t_tweetid tweetid, t.tweet tweet, t.tweetdate::text tweet_date, a.handle handle
                                                    FROM t_tweets t
                                                    INNER JOIN t_accounts a
                                                    ON a.id = t.acc_id
                                                    WHERE t.t_tweetid = '%s';" (get-in body ["refno"])) :results))
  )


(defn all-tweets-filter-by-date [body]
  ;update the api accesses table
  (with-db db-connection-accesses (exec-raw (format "INSERT INTO tbl_apicalls(subid) SELECT id FROM tbl_subscription WHERE apikey='%s';" (get-in body ["apikey"]))))
  (with-db db-connection-accesses (exec-raw (format "UPDATE tbl_apicalls SET iscorrect=1, postedstring='%s' WHERE id=(SELECT MAX(id) FROM tbl_apicalls);" (json/write-str body))))
  ;get all the records for the last 1 year
  (with-db db-connection-tweets (exec-raw (format "SELECT t.t_tweetid tweetid, t.tweet tweet, t.tweetdate::text tweet_date, a.handle handle
                                                    FROM t_tweets t
                                                    INNER JOIN t_accounts a
                                                    ON a.id = t.acc_id
                                                    WHERE t.tweetdate::date = '%s';" (get-in body ["tweetdate"])) :results))
  )


(defn current-weather [body]
  ;update the api accesses table
  (with-db db-connection-accesses (exec-raw (format "INSERT INTO tbl_apicalls(subid) SELECT id FROM tbl_subscription WHERE apikey='%s';" (get-in body ["apikey"]))))
  (with-db db-connection-accesses (exec-raw (format "UPDATE tbl_apicalls SET iscorrect=1, postedstring='%s' WHERE id=(SELECT MAX(id) FROM tbl_apicalls);" (json/write-str body))))
  ;get all the records for the last 1 year
  (with-db db-connection-weather (exec-raw (format "SELECT f_date fordate, f_time fortime, \"tempC\" tempc, \"tempF\" tempf, \"windspeedKmph\" windspeed, \"precipMM\" rainfall, \"HeatIndexC\" heatindex, \"FeelsLikeC\" feelslikec
                                                    FROM tbl_forecast_hourly_wwo w
                                                    INNER JOIN location l
                                                    ON l.id = w.city_id
                                                    WHERE w.city_id = 1 AND f_date = current_date::text
                                                    ORDER BY w.id asc LIMIT 8;" ) :results))
  )